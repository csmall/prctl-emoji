README for prct-emoji
=====================

This is a very simple and small program that will periodically change
its own process name.  It's used primarily for testing top but is very
silly in how it does it.

There is a demonstration of this at my [Mastodon toot](https://social.dropbear.xyz/@smallsees/99506299325857442)

To Use
------
make
./runname.sh

License
-------
This program is licensed under the BSD 3-clause license.
