/*
 * changename.c - Regularly change the process name
 *
 * Copyright 2018 Craig Small <csmall@dropbear.xyz>
 *
 * Licensed under the BSD 3-clause licence
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/prctl.h>

#define NUM_LINES 30
char *lines[NUM_LINES] = {
    "😐        ",
    " 😐       ",
    "  😐      ",
    "   😐     ",
    "    😐    ",
    "     😐   ",
    "      😐  ",
    "       😐 ",
    "        😨",
    "      😨🦁",
    "     😨🦁 ",
    "    😨🦁  ",
    "   😨🦁   ",
    "  😨🦁    ",
    " 😨🦁     ",
    "😨🦁      ",
    "🔥😱🦁    ",
    " 🔥😱🦁",
    "  🔥😱🦁",
    "   🔥😱🦁",
    "    🔥😱",
    "     🔥😱",
    "      🔥",
    "      🚒",
    "     🚒",
    "    🚒",
    "   🚒",
    "  🚒",
    " 🚒",
    "🚒",
};
int main(int argc, char *argv[])
{
    int i;

    sleep(1);
    for(i = 0; i < NUM_LINES; i++) {
        //printf("line: %s\n", lines[i]);
        prctl(PR_SET_NAME, lines[i], NULL, NULL, NULL);
        sleep(1);
    }

    return 1;
}
